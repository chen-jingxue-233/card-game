package com.example.cardgame

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

const val name="name"
const val sex="sex"
const val num="num"
const val phone="phone"
const val email="email"
const val hobby="hobby"
const val score="score"
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val button_edit=findViewById<Button>(R.id.edit_button)
        button_edit.setOnClickListener{
            val intent=Intent(this,InformationActivity::class.java)
            startActivityForResult(intent,0)
        }
        val game_button=findViewById<Button>(R.id.game_button)
        game_button.setOnClickListener{
            val intent=Intent(this,GameActivity::class.java)
            startActivityForResult(intent,1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==0)
        {
            if(resultCode==1)
            {
                val nameText=findViewById<TextView>(R.id.name2_text)
                val numText=findViewById<TextView>(R.id.number_text2_text)
                val phoneText=findViewById<TextView>(R.id.phone2_text)
                val emailText=findViewById<TextView>(R.id.email2_text)
                val sexText=findViewById<TextView>(R.id.sex2_text)
                val hobbyText=findViewById<TextView>(R.id.hobby2_text)
                val imageView=findViewById<ImageView>(R.id.imageView2)
                nameText.text=data?.getStringExtra(name)
                numText.text=data?.getStringExtra(num)
                phoneText.text=data?.getStringExtra(phone)
                emailText.text=data?.getStringExtra(email)
                sexText.text=data?.getStringExtra(sex)
                hobbyText.text=data?.getStringExtra(hobby)
                val bitmap = data?.getParcelableExtra<Bitmap>("bitmap")
                imageView.setImageBitmap(bitmap)
            }
        }else{
            if(resultCode==2)
            {
                val scoreText=findViewById<TextView>(R.id.score2_text)
                val a: Int = Integer.valueOf(scoreText.text.toString())
                val b:Int =Integer.valueOf(data?.getStringExtra(score))
                if(a<b)
                {
                    scoreText.text=data?.getStringExtra(score)
                }
            }
        }
    }
}