package com.example.cardgame

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.RadioButton
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import java.io.ByteArrayOutputStream


class InformationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_information)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val sure_button=findViewById<Button>(R.id.sure_button)
        val edit_name=findViewById<EditText>(R.id.edit_name)
        val edit_num=findViewById<EditText>(R.id.edit_num)
        val edit_phone=findViewById<EditText>(R.id.edit_Phone)
        val edit_email=findViewById<EditText>(R.id.edit_EmailAddress)
        val radioButton=findViewById<RadioButton>(R.id.radioButton)
        val radioButton2=findViewById<RadioButton>(R.id.radioButton2)
        val checkbox=findViewById<CheckBox>(R.id.checkBox)
        val checkbox2=findViewById<CheckBox>(R.id.checkBox2)
        val checkbox3=findViewById<CheckBox>(R.id.checkBox3)
        val checkbox4=findViewById<CheckBox>(R.id.checkBox4)
        val edit_photo=findViewById<Button>(R.id.edit_photo)
        edit_photo.setOnClickListener{
            val takePictureIntent= Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if(intent.resolveActivity(packageManager)!=null)
            {
                startActivityForResult(takePictureIntent,1)
            }
        }
        val imageView=findViewById<ImageView>(R.id.imageView)
        sure_button.setOnClickListener{
            val intent=Intent()
            intent.putExtra(name,edit_name.text.toString())
            intent.putExtra(num,edit_num.text.toString())
            intent.putExtra(phone,edit_phone.text.toString())
            intent.putExtra(email,edit_email.text.toString())
            if(radioButton.isChecked()){
                intent.putExtra(sex,radioButton.getText().toString())
            }else{
                intent.putExtra(sex,radioButton2.getText().toString())
            }
            if(checkbox.isChecked())
            {
                intent.putExtra(hobby,checkbox.getText().toString())
            }else if(checkbox2.isChecked()){
                intent.putExtra(hobby,checkbox2.getText().toString())
            }else if(checkbox3.isChecked()){
                intent.putExtra(hobby,checkbox3.getText().toString())
            }else{
                intent.putExtra(hobby,checkbox4.getText().toString())
            }

            intent.putExtra("bitmap",imageView.drawable.toBitmap())
            setResult(1,intent)
            finish()
        }
        val cancel_button=findViewById<Button>(R.id.cancel_button)
        cancel_button.setOnClickListener{
            finish()
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==1)
        {
            if(resultCode== RESULT_OK)
            {
                val imageBitmap=data?.extras?.get("data") as Bitmap
                val imageView=findViewById<ImageView>(R.id.imageView)
                imageView.setImageBitmap(imageBitmap)
            }
        }
    }
}