package com.example.cardgame

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.gridlayout.widget.GridLayout
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.children

class GameActivity : AppCompatActivity(),View.OnClickListener{
    lateinit var game:CardMatchingGame
    val cardButtons= mutableListOf<Button>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_game)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val gridLayout=findViewById<GridLayout>(R.id.gridlayout)
        for(v in gridLayout.children)
        {
            if(v is Button)
            {
                v.setOnClickListener(this)
                cardButtons.add(v)
            }
        }
        game= CardMatchingGame(cardButtons.count())
        updateUI()
        val recycle_button=findViewById<Button>(R.id.recycle_button)
        recycle_button.setOnClickListener{
            game=CardMatchingGame(cardButtons.count())
            updateUI()
        }
        val exit_button=findViewById<Button>(R.id.exit_button)
        val score_text=findViewById<TextView>(R.id.score_text)
        exit_button.setOnClickListener{
            val intent=Intent()
            intent.putExtra(score,score_text.text)
            setResult(2,intent)
            finish()
        }

    }
    fun updateUI(){
        for (button in cardButtons)
        {
            val index=cardButtons.indexOf(button)
            val card=game.cardAtIndex(index)
            button.isEnabled=!card.isMatched
            if(card.isChosen){
                button.text=card.toString()
                button.setBackgroundColor(Color.WHITE)
            }else{
                button.setBackgroundResource(R.drawable.photo)
            }
        }
        val score_text=findViewById<TextView>(R.id.score_text)
        score_text.text=String.format("%d",game.score)
    }
    override fun onClick(v:View?){
        if(v is Button)
        {
            val index=cardButtons.indexOf(v)
            game.chooseCardAtIndex(index)
            updateUI()
        }
    }
}